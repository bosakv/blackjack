import abc
from Deck import Deck
import random

class AbstractPlayer(abc.ABC):
    def __init__(self, position):
        self.cards = []
        self.position = position
        self.bet = 0




    def ask_card(self):
        card = deck.get_card()
        self.cards.append(card)
        return True

    @abc.abstractmethod
    def change_bet(self, max_bet, min_bet):
        pass


class Player(AbstractPlayer):


    def change_bet(self, max_bet, min_bet):
        while True:
            value = int(input("Make your bet! "))
            if value > max_bet and value < min_bet:
                self.bet = value
                break

class Dealler(AbstractPlayer):
    pass

class Bot(AbstractPlayer):


    def change_bet(self, max_bet, min_bet):
        self.bet = random.randint(min_bet, max_bet)


