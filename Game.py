import random

import Player
from Deck import Deck
from const import MESSAGE

class Game:
    def __init__(self):
        self.players = []
        #self.dealler = None
        self.player = None
        self.all_players_count = 1
        self.deck = Deck()
        self.max_bet, self.min_bet = 20, 0

    
    @staticmethod
    def _ask_starting(message):

        while True:
            choise = input(message)
            if choise == "n":
                return False
            elif choise == "y":
                return True


    def _launching(self):
        bots_count = int(input("Hello, write bots count: "))
        self.all_players_count = bots_count + 1
        for i in range(bots_count):
            b = Player.Bot(position=i)
            self.players.append(b)
            print(b, "is created")
        self.player = Player.AbstractPlayer(position=bots_count + 1)

    def ask_bet(self):
        for player in self.players:
            player.change_bet(self.max_bet, self.min_bet)

    def start_game(self):
        message = MESSAGE.get("ask_start")
        if not self._ask_starting(message=message):
            exit(1)

        self._launching()
        self.ask_bet()



